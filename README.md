## Introduction

Logmessage Scribe reads a git patch and produces a template for a
logmessage, which you can then use to write a great logmessage that
explains the rationale behind your work.

See some [sample output](https://bitbucket.org/logmessage_scribe/logmessage-scribe/src/344db1f4d6dfed3baf966dbeff969bbebc988100/sample.output?at=master) which was produced from this [sampler.patch](https://bitbucket.org/logmessage_scribe/logmessage-scribe/src/344db1f4d6dfed3baf966dbeff969bbebc988100/sampler.patch?at=master).

Logmessage Scribe currently works for C and Cmake (very rudimentary)
but will soon also work for python and java. It will also work for svn
patches soon.

Tips on writing good logmessages can be found in the [Subversion Community Guide](https://subversion.apache.org/docs/community-guide/conventions.html#log-messages)


## Google Apps

Logmessage Scribe is a hosted webapp on Google Appengine:

[http://logmessage-scribe.appspot.com/](http://logmessage-scribe.appspot.com/)


## Contribute

If you'd like to contribute, please send email to:
logmessagescribe@gmail.com

Current plans:
    
  * write the python module
  * write the java module
  * cater for svn diffs

Other language modules can also be added easily.


## How to run and test
```
scribe.py        --- commandline tool.  
                     $ ./scribe.py -p path/to/patch

test_scribe.py   --- run all the current tests
                     $ ./test_scribe.py

run_test.py      --- container for running a single test, instead of the
                     entire test_scribe.py script.
                     Useful if you like to use coverage:
                     http://nedbatchelder.com/code/coverage/
```
## How this is built

```
scribe.py            --- Commandline tool that calls diff_translator.py

logmessage-scribe.py --- Google Apps main page tool and input form.

diff_translator.py   --- Splits the patch in lines and decides 
                         which language module to use.

make_headers.py      --- provides a fake header for different languages for 
                         the tests. 

c_section.py         --- translates C parts of a patch

cmake_section.py     --- translates CMake parts of a patch

python_section.py    --- translates python parts of a patch
```


## Additional Information


This is written in Python 2.7 because Google Apps does not support
Python 3 yet.  Should not matter all that much, updating it
automagically just results in the print statements being updated.
