## we route text to be processed according to what is in the file extension.
def python_snippet(snippet):
    """Add python file header to patch snippet, return as []"""
    blob = """
--- a/python.py
+++ b/python.py
    """ + snippet

    return blob.split('\n')


def cmake_snippet(snippet):
    """Add cmake file header to patch snippet, return as []"""
    blob = """
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
    """ + snippet

    return blob.split('\n')


def c_code_snippet(snippet):
    """Add c code file header to patch snippet, return as []"""
    blob = """
--- a/C_code.c
+++ b/C_code.c
    """ + snippet

    return blob.split('\n')


def c_header_snippet(snippet):
    """Add c header file header to patch snippet, return as []"""
    blob = """
--- a/C_header.h
+++ b/C_header.h
    """ + snippet

    return blob.split('\n')
