import re

# text offset of 4 spaces
OFFSET = '    '


def not_a_eligible_patch_line(text):
    """it's a comment, or a changed line inside a function (may be empty))"""
    if text[:3] in ['-  ', '+  ', '+//', '-//', '+/*', '-/*']:
        return True
    return False


def is_end_of_comment(text):
    """ end of a comment */ """
    if '/*' not in text and '*/' in text:
        return True
    return False


def remove_comments(text):
    """remove embedded comments """
    if '/*' in text and '*/' in text:
        return text[0:text.index('/*')]+text[text.index('*/')+2:]

    if '//' in text:
        return text[0:text.index('//')]

    return text


def remove_stuff_in_curly_braces(line):
    """ remove anything in curly braces that follows the = sign"""

    if "{" in line and "}" in line:
        line = line[0:line.index('{')]+line[line.index('}')+1:]
        line = remove_stuff_in_curly_braces(line)
    return line


def remove_strings_between_double_quotes(line):

    if "=" not in line: # or "char" not in line:
         return line

    # I didn't write this nifty item.  I borrowed here:
    # https://mail.python.org/pipermail/tutor/2003-December/027063.html
    # it still fails on stings quoted inside of strings.
    line = line.replace("'",'"')
    regex = re.compile(r'(?:"[^"]*\\(?:.[^"]*\\)*.[^"]*")|(?:"[^"]*")')
    all_strings = regex.findall(line)

    for p in range(0, len(all_strings)):
        line = line.replace(all_strings[p],"")

    ll = line.split()
    for p in range(1, len(ll)):
        if ll[p-1] == "=" and not ll[p].isalnum():
            ll[p-1] = " "
    line = ' '.join(ll)
    line = line.replace("  ","")
    line = line.replace(" ;",";")
    return line



def is_a_label(text):
    """Omit labels"""
    if len(text) > 3:
        if text[-1] == ':' or text[-2] == ':':
            return True
    return False


def add_or_remove_action(line):
    """ decide if this line is an addition or a removal in the patch"""
    action = None

    if line[0] == '+':
        action = 'Add'

    elif line[0] == '-':
        action = 'Remove'

    return action


def is_this_a_function(line, current_topic, types, result):
    """ Are we dealing with a function here?  """

    if line[0] != '@':
        return (result, current_topic)

    words = line.split()

    # check if this is a change inside a function
    # @@ turns up elsewhere, but the function context
    # has at least 5 words
    if len(words) > 5:
        done = False
        # some functions have type modifiers so we need
        # to weed those out
        for j in range(5, len(words)):
            if '(' in words[j]:
                function_name = words[j].split('(')[0]
            elif '{' in words:
                # we have a modified typedef/ struct
                 if 'typedef' in words or 'struct' in words:
                     brace_contents = []
                     paren_location = words.index('{')
                     for k in range(4, paren_location):
                         brace_contents.append(words[k])
                         if k < paren_location-1:  # ensure there is no ugly space at the end
                             brace_contents.append(" ")

                     function_name = ''.join(brace_contents)
            else:
                 function_name = ''

            if current_topic == function_name:
                done = True
                continue
            elif function_name != '' and function_name not in types and not done :
                if function_name[0] == "*" :
                    function_name = function_name[1:]

                current_topic = function_name
                tail_text = '): Modified function.\n'
                result.append('\n' + OFFSET + '('+
                              function_name + tail_text)

                done = True
                continue

    return (result, current_topic)


def remove_patch_plus_or_minus(text):
    """ zap the + or - at the beginning of a line"""
    return text[1:]


def was_function_removed_or_added(raw_line, action, result):
    """ Add some blurb, depnding on wether the function is new or removed. """

    function_name = ''.join(raw_line).split('(') # find the name to

    if len(function_name) > 1 and function_name[1][:2].isalnum():
        if action == 'Add':
            advice = '): New function.\n'
        else:
            advice = '): Removed.\n'
        snippet = function_name[0].split(' ')

        function_n = snippet[-1]
        if len(function_n) > 0 and function_n[0] == "*" :
            function_n = function_n[1:]

        result.append('\n' + OFFSET + '(' +
                          function_n + advice)
        return (True, action, result)
    return (False, action, result)


def is_hash_statements(first_word, current_topic, words, result, action):
    """ Deal with hash statements -- anything after a '#'. """

    if first_word in ['#endif', '#else', '#elseif']:
        return (True, result, current_topic)
    elif first_word in ['#if', '#ifndef', '#ifdef', '#include',
                      '#define', '#pragma']:
        if current_topic != words[0]:
            current_topic = words[0]
            result.append('\n' + OFFSET + '(' + words[0][1:] + '): ' +
                          action + ' '+words[1]+ '.' + '\n')
        else:
            result.append(OFFSET+'            ' + action + ' ' +
                          words[1] + '.' + '\n')
        return (True, result, current_topic)
    else:
        return (False, result, current_topic)


def check_name_in_types_maybe_add(first_word, types):
    """ Add the name to [types]  array if it looks eligible. """
    # ... by now we have excluded all other possibilities. So, if it
    # isn't in types, we can now check if it's a typedef'ed
    # custom name.
    fw_len = len(first_word)
    if first_word not in types:
        if (first_word and first_word not in
            [';', '{', '}', ' ', '};', '/', '*/', '}*/']):
            types += [first_word] ## temporary addition to types
    return (fw_len, types)


def collect_identifier_types(first_word, words, types):
    """  deal with more than one type identifier, eg. static int """
    ## step through the words[] and collect all types, for cases like
    # 'unsigned long long' etc
    identifier = first_word
    w = 1
    while len(words) > w and words[w] in types:
        identifier = identifier + ' ' + words[w]
        w += 1

    return(identifier, w)


def pretty_print_identifiers(current_topic, identifier, words, types, result, w, action):
    """add the identifier and pretty print to the correct OFFSET """

    if current_topic != identifier: # our first time for this group
        current_topic = identifier
        if 'struct' in current_topic or 'typedef' in current_topic:
            if len(words) > w and words[w][1:].isalnum():
                identifier += " " + words[w]
                result.append('\n' + OFFSET + '(' + identifier +
                              '): Modifed or removed.\n')
                return (True, types, identifier, result)
        else:
            if words[w-1] in types and not (words[w].endswith(';') or words[w].endswith(',')):
                identifier = identifier + ' ' + words[w]
                types.append(words[w])
        result.append('\n' + OFFSET + '(' + identifier + '): '
                      + action + ' ')
    else: #we have repeated declarations on one line
        # magic number 3: (+)+:+' '
        result.append(OFFSET + ' ' * int(len(identifier)+4) +
                      action + ' ')
    return (False, types, identifier, result)


def typeset_variable_list(line, types):
    """ collect all the eligible variables for typesetting """
    # remove entries that are special characters to
    # accommodate inline assigned variables and multiple
    # variables on one line.
    var = ''.join(re.split(r'[=;,{\n]', line)).split()
    res = []
    for j in range(1, len(var)):

        if var[j][0] in ["*"] :
            var[j] = var[j][1:]

        if not var[j].isdigit() and var[j] not in types:
            # exclude declared strings
            addVar = True
            for c in var[j]:  #dangerous, remove!
                if c in ['"',"'"]:
                    addVar = False
                    break
            if addVar:
                res += [var[j]]
    return (res, len(res))


def handle_struct_or_typedef(c_section, i, result):
    """typedfs and structs can be named on the first or on the last line """

    complete = False
    line_iterator = i
    # look forward in the line list for the name.
    while (len(c_section) > line_iterator and
           len(c_section[line_iterator]) > 0 and
           not complete):

        contents = c_section[line_iterator].split()

        if len(contents) == 1: # careful: this may not
                               # deal with renamed
                               # structs.  Rethink.
            line_iterator += 1
        else:
            if len(contents) < 1:
                line_iterator += 1
                return (True, result)

            con_len = len(contents[1])-1
            # check that there is a word between a } and ;
            # careful:, this does not deal with } a = b;
            # assignments
            if contents[0] == '+}' and contents[1][con_len] == ';':
                result.append(str(contents[1][0:con_len]) + '.\n')
                complete = True
            else:
                line_iterator += 1
                # this happens if there was a renaming going on
                if line_iterator >= len(c_section):
                    complete = True
    if complete:
        return (True, result)
    return (False, result)


def thread_variables_and_pretty_print(size, result, identifier, action, res):
    """  Take a list of variables we have collected and pretty print them. """
    ## we trivially have a variable name, thread them
    ## into a sentence, seperated by commas, ending in a
    ## full stop.
    for i in range(0, size):
        if i < size-1:
            result.append(res[i] + ',\n'+
                          (OFFSET +
                           ' ' *
                           int(len(identifier)+4+len(action)+1)))
        else:
            result.append(res[i] + '.\n')
            return True

    return False


def generator(c_section, current_topic, summary):
    """Parse a c code patch file as a log message"""

    ## note: reparse for summary after build is complete.
    # if summary:
    #    tail_text = '): Modified function.\n'
    # to '): .' ready for convenient editing by user.
    ########

    types = ["struct", "int", "char", "signed", "unsigned", "short", "long",
             "float", "double", "enum", "typedef", "void", "static","extern",
             "asm","register","volatile","inline","const","union","auto",
             "__attribute__","*const","+const"]

    result = []

    for i, line in enumerate(c_section):

        if not_a_eligible_patch_line(line) or is_end_of_comment(line):
            continue

        line = remove_stuff_in_curly_braces(line)

        line = remove_strings_between_double_quotes(line)

        line = remove_comments(line)

        if is_a_label(line) or not line:
            continue

        action = add_or_remove_action(line)
        if action == None:
            (result, current_topic) = is_this_a_function(
                line, current_topic, types, result)
            continue

        words = line.split()

        if len(words) > 0:
            first_word = remove_patch_plus_or_minus(words[0])
        else:
            continue # nothing in in this line.

        (goto_next_line, action, result) = was_function_removed_or_added(
            c_section[i], action, result)
        if goto_next_line:
            continue

        (goto_next_line, result, current_topic) = is_hash_statements(
            first_word, current_topic, words, result, action)
        if goto_next_line:
            continue

        (fw_len, types) = check_name_in_types_maybe_add(first_word, types)

        ## Check if we are dealing with a global variable
        if first_word in types:

            (identifier, w) = collect_identifier_types(first_word, words, types)

            (goto_next_line, types, identifier, result) = pretty_print_identifiers(
                current_topic, identifier, words, types, result, w, action)
            if goto_next_line:
                continue

            (res, size) = typeset_variable_list(line, types)

            if size == 0: # we didn't find a name for the variable
                (goto_next_line, result) = handle_struct_or_typedef(
                    c_section, i, result)
            else:
                goto_next_line = thread_variables_and_pretty_print(
                    size, result, identifier, action, res)
                if goto_next_line:
                    continue

    return result
