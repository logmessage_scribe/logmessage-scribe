#!/usr/bin/python
"""Simple python module to produce template log message."""

import getopt
import os
import re
import sys
import diff_translator

DEBUG_LEVEL = 17

def debug_print(level, text):
    """debug helper function"""
    if level == DEBUG_LEVEL:
        print text


def show_help():
    """Help message function."""
    print "Logmessage Scribe produces a log message template."
    print "\nOptions:"
    print "-p patch file  -- required."
    print "-o output file -- optional, otherwise output goes to stdio."
    print "-s summary     -- produce a summary instead of a template.\n"
    print "Currently only git diff is supported."
    print "The scribe deals with C, if it works for other languages,"
    print "that is entirely accidental.  Planned support:"
    print "CMake, python, standard diff, svn diff."

def load_in_file(inputfile):
    """Function to read file and return contents.

    Input:
        inputfile: Name of patch file.
    Output:
        lines: array of lines
    Potential improvement: check that this file contains a (git) diff.
    """
    lines = []
    with open(inputfile, 'r') as the_file:
        lines = the_file.readlines()
    return lines


def main(argv):
    ''' Generate a log message template from a patch file  '''

    outputfile = ''
    summary = False

    try:
        opts, args = getopt.getopt(argv, "h:p:o:s",
                                   ["help","patchfile=", "output=", "summary"])
    except getopt.GetoptError:
        show_help()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            show_help()
            sys.exit()

        elif opt in ("-p", "--patchfile"):

            input_text = load_in_file(arg)
            if input_text == []:
                print ("The file you specified could not be found " +
                       "or is not readable.\n")
                sys.exit()

        elif opt in ("-o", "--output"):
            outputfile = arg

        elif opt in ("-s", "--summary"):
            summary = True


    status = ""
    input_text = diff_translator.remove_tabs(input_text, 8)

    result = diff_translator.make_logmessage_template(input_text, summary)

    if outputfile != '':
        logmessage_file = open(outputfile, "w")
        logmessage_file.write(result)
        logmessage_file.close()
        print "Your log message template was written to "+outputfile+".\n"
    else:
        print result

    print status

    sys.exit()


if __name__ == "__main__":
    main(sys.argv[1:])
