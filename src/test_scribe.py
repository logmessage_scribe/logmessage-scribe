#!/usr/bin/python
import sys
import diff_translator
import unittest
import make_headers

def show_result(result):
    """Show a result with nice formatting in repr and printf state"""
    ## grab the repr result to paste into the test.
    print "\n=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+"
    print "    "+sys._getframe(1).f_code.co_name
    print "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+\n"
    print repr(result)
    print "--------------------------------------------------------------------------------"
    print result
    print "\n=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+\n"


class SRTest(unittest.TestCase):

    ## headers needed to route the patch fragment to the correct
    ## language translator

    def test_empty_patch(self):
        """Test the empty patch file"""
        patch_lines = []
        result = diff_translator.make_logmessage_template(patch_lines)
        self.assertFalse(result)

################################################################################
###########################   C/C++ tests   ####################################
################################################################################


    def test_hash_includes_section_file_name(self):
        patch_lines = make_headers.c_header_snippet("""
@@ -21,6 +21,9 @@
+#pragma once
 #include "ODFPackage.h"
 #include "ODFTextConverter.h"
 #include "DFDOM.h"
+#include "DFHTML.h"
+#include "DFHTMLNormalization.h"
-#include "CSS.h"
+#include "../../foo/bar/baz.h"
""")
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals('\n\n  * C_header.h:\n\n'
                          '    (#pragma): Add once.\n\n'
                          '    (#include): Add "DFHTML.h".\n'
                          '                Add "DFHTMLNormalization.h".\n\n'
                          '    (#include): Remove "CSS.h".\n\n'
                          '    (#include): Add "../../foo/bar/baz.h".\n',
                          result,
                          'Fail to display addition or removal.')

    def test_comment_handling(self):
        patch_lines = make_headers.c_header_snippet("""
-
+
+// argle gargle blopf
-// ribbit foo
+/* yadda
-/* flobble
+rabbit */
+int i, j, k,  /* a comment */ q = 23, e;
+int s = 34, /* this is a comment */ t = 35, q;
-char foo = "wibble"; // a comment
+char bar = 'wobble' /* a comment */
+int u = 39, /* // embbedded comment */ v = 36, z = 2;
-int r = 34; /* this is a comment */
-int w = 40; // end comment
    """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (int): Add i,\n'
                          '               j,\n'
                          '               k,\n'
                          '               q,\n'
                          '               e.\n'
                          '\n'
                          '    (int): Add s,\n'
                          '               t,\n'
                          '               q.\n'
                          '\n'
                          '    (char): Remove foo.\n'
                          '\n'
                          '    (char): Add bar.\n'
                          '\n'
                          '    (int): Add u,\n'
                          '               v,\n'
                          '               z.\n'
                          '\n'
                          '    (int): Remove r.\n'
                          '\n'
                          '    (int): Remove w.\n',
                          'Comment handling failed.')


    def test_inserted_blank_handling(self):
        patch_lines = make_headers.c_header_snippet("""
-
+
    """)

        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result, '', 'Inserted blank alert failed.')

    def test_actions_modified_parameterlist_and_function(self):
        """Paramlist modfied, function modfied, no duplication."""
        patch_lines = make_headers.c_header_snippet("""
-DFDocument *ODFTextGet(DFStorage *concreteStorage, DFStorage *abstractStorage, const char *idPrefix, DFError **error)
+DFDocument *ODFTextGet(ODFConverter *converter)
@@ -95,26 +123,53 @@ DFDocument *ODFTextGet(DFStorage *concreteStorage, DFStorage *abstractStorage, c
@@ -122,7 +139,7 @@ DFDocument *ODFTextGet(DFStorage *concreteStorage, DFStorage *abstractStorage, c
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n\n'
                          '  * C_header.h:\n\n'
                          '    (ODFTextGet): Parameter list was modified.\n\n'
                          '    (ODFTextGet): Modified function.\n',
                          'Modified function handling failed.')


    def test_new_function_added(self):
        patch_lines = make_headers.c_header_snippet("""
+void show_nodes(DFNode *odfNode, int level)
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        #show_result(result)
        self.assertEquals(result,
                          '\n\n'
                          '  * C_header.h:\n\n'
                          '    (show_nodes): New function.\n',
                          'New function added failed.')


    def test_function_removed(self):
        patch_lines = make_headers.c_header_snippet("""
-void show_nodes(DFNode *odfNode, int level)
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n\n  * C_header.h:\n\n    (show_nodes): Removed.\n',
                          'Function removed failed.')


    def test_single_hash_defines(self):
        patch_lines = make_headers.c_header_snippet("""
+#ifndef __FOO
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n\n  * C_header.h:\n\n    (#ifndef): Add __FOO.\n',
                          'Single Hash defines.')


    def test_exclude_hash_defines(self):
        patch_lines = make_headers.c_header_snippet("""
+#endif //FOO',"#else /* some comment */","#elseif // bar baz
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '',
                          'Repress excluded hash defines.')

    def test_string_assignment_in_var_list(self):
        patch_lines = make_headers.c_header_snippet("""
+char a1, a2, a3 = 'yodel', a4[] = { 'a', 'b'};
+char b1, b2, b3 = "yodel", b4[] = { "a", "b"};
+my_typedef_string c1, c2, c3[] = { "a", "b"};
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (char): Add a1,\n'
                          '                a2,\n'
                          '                a3,\n'
                          '                a4[].\n'
                          '\n'
                          '    (char): Add b1,\n'
                          '                b2,\n'
                          '                b3,\n'
                          '                b4[].\n'
                          '\n'
                          '    (my_typedef_string): Add c1,\n'
                          '                             c2,\n'
                          '                             c3[].\n',
                          'Strings in var list failure.')


    def test_long_var_names(self):
        patch_lines = make_headers.c_header_snippet("""
-short the_road_is_looooog, the_road_is_looooog2, the_road_is_looooog3, the_road_is_looooog4, the_road_is_looooog5;
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (short): Remove the_road_is_looooog,\n'
                          '                    the_road_is_looooog2,\n'
                          '                    the_road_is_looooog3,\n'
                          '                    the_road_is_looooog4,\n'
                          '                    the_road_is_looooog5.\n',
                          'List &wrap repeated variables with 1 or more type.')



    def test_qualifier_not_in_types(self):
        patch_lines = make_headers.c_header_snippet("""
+struct structB = {
+    int foo;
+    char bar;
+};
+typedef struct predeclare;
+structA my_array[];
+typedef struct = {
+    int foo;
+    char bar;
+} structA;
""")
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (struct structB): Modifed or removed.\n'
                          '\n'
                          '    (typedef struct): Add predeclare.\n'
                          '\n'
                          '    (structA): Add my_array[].\n'
                          '\n'
                          '    (typedef struct): Add structA.\n',
                          'List and wrap global variables not in types')


    def test_qualifier_in_types(self):
        patch_lines = make_headers.c_header_snippet("""
+const char *const c = "foobar";  /* embedded "*" */
+const char *const yadda;
+*const int const *wibble;
-someType something;
-const someType something;
+const someType const* something;
+unsigned long long ribbit;
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (const char *const): Add c.\n'
                          '\n'
                          '    (const char *const): Add yadda.\n'
                          '\n'
                          '    (*const int const): Add wibble.\n'
                          '\n'
                          '    (someType): Remove something.\n'
                          '\n'
                          '    (const someType): Remove something.\n'
                          '\n'
                          '    (const someType const*): Add something.\n'
                          '\n'
                          '    (unsigned long long): Add ribbit.\n',
                          'List &wrap repeated variables with 1 or more type.')


    def test_empty_line_bug(self):
        patch_lines = make_headers.c_header_snippet("""
#ifndef DocFormats_ODF_h
 #define DocFormats_ODF_h
\t\targle

+#include <DocFormats/DFError.h>\n"
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertTrue('DocFormats/DFError.h' in result,
                        'Include file not found in message')

    def test_remove_contents_between_curly_braces(self):
        patch_lines = make_headers.c_header_snippet("""
+char bar = {"one","two","three four" };
+int foo[] = { 1, 2, 3, 4, 6, 7 };
    """)

        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (char): Add bar.\n'
                          '\n'
                          '    (int): Add foo[].\n',
                          'Remove curly braces failed.')


    def test_omit_labels(self):
        patch_lines = make_headers.c_header_snippet("""
+    ok = 1;
+
+end:
+    ODFPackageRelease(package);
""")
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertTrue('end:' not in result,
                        'Omit labels from template.')


    def test_handle_inline_ending_comments_omit(self):
        patch_lines = make_headers.c_header_snippet("""
+int foo; comment */\n
        """)
        # show_result(result)
        result = diff_translator.make_logmessage_template(patch_lines)
        self.assertTrue('(int): Add foo' not in result,
                        'handle ending comments correctly: omit.')


    def test_change_in_struct(self):
        patch_lines = make_headers.c_header_snippet("""
typedef struct ODFPackage ODFPackage;

@@ -34,7 +33,7 @@ struct ODFPackage {
         DFDocument *settingsDoc;
         DFDocument *stylesDoc;
         ODFManifest *manifest;
    -    ODFSheet *sheet;
    +
};
            """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                        '\n\n  * C_header.h:\n\n    (struct ODFPackage): Modified function.\n',
                        'Change in struct correctly assessed.')



    def test_second_identifiers_in_types(self):
        patch_lines = make_headers.c_header_snippet("""
+char *__attribute__ ((aligned(8))) *f;
+typedef State Grid[MAXROW+2] [MAXCOL+2]
+typedef enum state {DEAD,ALIVE} State
 #include "DFXML.h"
            """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (char): Add ((aligned(8))),\n'
                          '                f.\n'
                          '\n'
                          '    (typedef State): Modifed or removed.\n'
                          '\n'
                          '    (typedef enum state): Modifed or removed.\n',
                        'TODO')


    def test_modified_parameter_list(self):
        patch_lines = make_headers.c_header_snippet("""
-void show_nodes(DFNode *odfNode)
+void show_nodes(DFNode *odfNode, int level)
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertTrue('\n     (show_nodes): Parameter list was modified.\n',
                        'Flag modified parameterlist.')

    def test_quoted_text_removal(self):
        patch_lines = make_headers.c_header_snippet("""
+char a = "Yak make-up and manicure";
+char b = "Yak", b1 = "and a manicure", b2;
    """)

        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (char): Add a.\n'
                          '\n'
                          '    (char): Add b,\n'
                          '                b1,\n'
                          '                b2.\n',
                          'String removal failed.')


    def test_def__vars(self):
        patch_lines = make_headers.c_header_snippet("""
+#ifndef BITS
+#error This file should only be repeatedly #included by dt_module.c.
+#endif
+
+#define JOIN(pre,post) pre##post
+#define EXJOIN(pre,post) JOIN(pre,post)
+#define JOINMID(pre,mid,post) pre##mid##post
+#define EXJOINMID(pre,mid,post) JOINMID(pre,mid,post)
+#define BITIZE(pre) EXJOIN(pre,BITS)
+#define ElfIZE(suffix) EXJOINMID(Elf,BITS,_##suffix)
+#define ELFIZE(suffix) EXJOINMID(ELF,BITS,_##suffix)

+static uint_t BITIZE(dt_module_syminit)(dt_module_t *dmp)
{
        const ElfIZE(Sym) *sym = dmp->dm_symtab.cts_data;
/*...*/
}
/*...*/
+static int BITIZE(dt_module_symcomp)(const void *lp, const void *rp, void *strtabp)
{
        ElfIZE(Sym) *lhs = *((ElfIZE(Sym) **)lp);
        """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (#ifndef): Add BITS.\n'
                          '\n'
                          '    (#error): Add This,\n'
                          '                  file,\n'
                          '                  should,\n'
                          '                  only,\n'
                          '                  be,\n'
                          '                  repeatedly,\n'
                          '                  #included,\n'
                          '                  by,\n'
                          '                  dt_module.c..\n'
                          '\n'
                          '    (JOIN): New function.\n'
                          '\n'
                          '    (EXJOIN): New function.\n'
                          '\n'
                          '    (JOINMID): New function.\n'
                          '\n'
                          '    (EXJOINMID): New function.\n'
                          '\n'
                          '    (BITIZE): New function.\n'
                          '\n'
                          '    (ElfIZE): New function.\n'
                          '\n'
                          '    (ELFIZE): New function.\n'
                          '\n'
                          '    (BITIZE): New function.\n',

                        '#defines and #error correctly recognised.')


    def test_const_vars(self):
        patch_lines = make_headers.c_header_snippet("""
+const char *const b = "foobar";  /* embedded "*" */
+const char *const x, y, z;
+const unsigned static char c = "foobar";  /* embedded "*" */
+const char d;  /* embedded "*" */
+const foo_baton_t bar;    /* typedef */
+const foo_baton_t bar;
+const foo_baton_t bar, baz, boing;;
            """)
        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (const char *const): Add b.\n'
                          '\n'
                          '    (const char *const): Add x,\n'
                          '                             y,\n'
                          '                             z.\n'
                          '\n'
                          '    (const unsigned static char): Add c.\n'
                          '\n'
                          '    (const char): Add d.\n'
                          '\n'
                          '    (const foo_baton_t): Add bar.\n'
                          '\n'
                          '    (const foo_baton_t): Add bar.\n'
                          '\n'
                          '    (const foo_baton_t): Add bar,\n'
                          '                             baz,\n'
                          '                             boing.\n',
                        'var quantifiers correctly separated.')


################################################################################
###########################   cmake tests   ####################################
################################################################################

    def test_simple_cmake_patch(self):
        patch_lines = make_headers.cmake_snippet("""
@@ -9,11 +9,14 @@ set (Tutorial_VERSION_MINOR 0)
 configure_file (
   "${PROJECT_SOURCE_DIR}/TutorialConfig.h.in"
   "${PROJECT_BINARY_DIR}/TutorialConfig.h"
+  "${PROJECT_BINARY_DIR2}/testing.h"
   )

 # add the binary tree to the search path for include files
 # so that we will find TutorialConfig.h
 include_directories("${PROJECT_BINARY_DIR}")
+include_directories("${PROJECT_BINARY_DIR2}")

 # add the executable
 add_executable(Tutorial tutorial.cxx)
-add_executable(Tutorial testing.cxx)
            """)

        result = diff_translator.make_logmessage_template(patch_lines)
        # show_result(result)
        self.assertEquals(
            '\n\n'
            '  * CMakeLists.txt:\n\n'
            '    (PROJECT_BINARY_DIR2): Added.\n\n'
            '    (include_directories): Added.\n\n'
            '    (add_executable): Removed.\n',
            result,
            'Test simple cmake case.')


################################################################################
##########################   python tests   ####################################
################################################################################

#     def test_python_import_collation(self):
#         patch = python_snippet("""
# @@ -1,5 +1,8 @@
# #!/usr/bin/python
# import re
# +import c_section
# +import cmake_section
# +import python_section
# -import removed_section
# -import other_section
#         """)
#         result = diff_translator.make_logmessage_template(patch)
#         # show_result(result)
#         self.assertEquals(result,
#                           '\n\n  * CMakeLists.txt:\n\n    (PROJECT_BINARY_DIR2):'
#                           ' Added.\n\n    (include_directories): Added.\n\n    '
#                           '(add_executable): Added.\n',
#                           'Collate import statements.')


    def test_place_holder(self):
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
