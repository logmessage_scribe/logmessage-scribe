import cgi
from google.appengine.api import users
import webapp2
import re
import diff_translator

#!/usr/bin/python
"""Simple python module to produce template log message."""

MAIN_PAGE_HTML = """\
<html>
  <body>
      <div><H3>The Logmessage Scribe</h3></div>
      <div>Turn your patch into a ready-made logmessage template.  [Currently supported: C, Cmake (rudimenary)  Soon: Python]<br/><br/></div>
      <div>Paste your git diff patch here:</div>

      <form action="/store" method="post">
      <div><textarea name="content" rows="20" cols="100"></textarea></div>
      <div>
          <input type="submit" value="Make the template">
      </div>
    </form>
    <div><a href="https://subversion.apache.org/docs/community-guide/conventions.html#log-messages">
          The Subversion Project advice on how to write a great logmessage.</a></div>
    <hr>
    <div><a href="http://gabriela-gibson.blogspot.com">
          Visit my Coding Diary!</a></div>

  </body>
</html>
"""


class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.write(MAIN_PAGE_HTML)


class input_form(webapp2.RequestHandler):
    def post(self):
        cont = cgi.escape(self.request.get('content')).split("\n")
        cont1 = diff_translator.remove_tabs(cont)
        cont2 = diff_translator.make_logmessage_template(cont1)
        self.response.write('<html><body><pre>')
        self.response.write(cont2)
        self.response.write('</pre></body></html>')


app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/store', input_form),
], debug=False)
