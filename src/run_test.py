#!/usr/bin/python
import sys
import diff_translator
import unittest
import make_headers

def show_result(result):
    """Show a result with nice formatting in repr and printf state"""
    ## grab the repr result to paste into the test.
    print "\n=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+"
    print "    "+sys._getframe(1).f_code.co_name
    print "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+\n"
    print repr(result)
    print "--------------------------------------------------------------------------------"
    print result
    print "\n=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+\n"




class SRTest(unittest.TestCase):

    def test_const_vars(self):
        patch_lines = make_headers.c_header_snippet("""
+const char *const b = "foobar";  /* embedded "*" */
+const char *const x, y, z;
+const unsigned static char c = "foobar";  /* embedded "*" */
+const char d;  /* embedded "*" */
+const foo_baton_t bar;    /* typedef */
+const foo_baton_t bar;
+const foo_baton_t bar, baz, boing;;
            """)
        result = diff_translator.make_logmessage_template(patch_lines)
        show_result(result)
        self.assertEquals(result,
                          '\n'
                          '\n'
                          '  * C_header.h:\n'
                          '\n'
                          '    (const char *const): Add b.\n'
                          '\n'
                          '    (const char *const): Add x,\n'
                          '                             y,\n'
                          '                             z.\n'
                          '\n'
                          '    (const unsigned static char): Add c.\n'
                          '\n'
                          '    (const char): Add d.\n'
                          '\n'
                          '    (const foo_baton_t): Add bar.\n'
                          '\n'
                          '    (const foo_baton_t): Add bar.\n'
                          '\n'
                          '    (const foo_baton_t): Add bar,\n'
                          '                             baz,\n'
                          '                             boing.\n',
                        'var quantifiers correctly separated.')


if __name__ == '__main__':
    unittest.main()
