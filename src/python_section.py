import re

def generator(python_section, current_topic, summary):
    """Parse the patch of a python file"""

    action = ''

    result = []

    offset = '    '

    for i, line in enumerate(python_section):

        ### create c section #################################################
        ##### Comment handling: Exclude comments and lines inside of functions
        if len(line) < 2:
            continue

        if line[:3] in ['#']:
            continue

        words = re.compile('\w+').findall(line)
        # print words
        if line[0] == '+':
            action = 'Added.'
        elif line[0] == '+':
            action = 'Removed.'
        elif line[0] == '@' and len(words) > 5 : # check if this is a change inside a function
            # @@ turns up elsewhere, but the function context
            # has at least 5 words
            ind = words.index('def')
            print line
            if ind != -1:
                result.append('\n' + offset+'(' + words[ind+1] + '): .\n')
            continue
        else:
            continue
        if len(words) > 0 and words[0] in ['def','import']:
            result.append('\n' + offset+'(' + words[0] + '): '+ action + '\n')

    return result
