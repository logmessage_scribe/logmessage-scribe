#!/usr/bin/python
import re
import c_section
import cmake_section
import python_section


def remove_tabs(input_lines, tabsize=8):
    """ Remove all tabs from document and replace with tabsize spaces

    Input:
        input_lines: Array of lines, optional tabsize value

    Output:
       input_lines: Array of lines
    """

    result = []
    for line in input_lines:
        expanded_line = line.expandtabs(tabsize)
        if len(expanded_line) > 0:
            result.append(expanded_line)

    return result



def make_logmessage_template(inputfile, summary=True):
    """Generate a log message template.

    Input:
       inputfile: list of strings.
                  The patch file contents as a list of lines.
    Output:
       result: A plain string containing the template.
    """

    inputfile = remove_tabs(inputfile)
    ## inputfile = remove_new_lines(inputfile)
    ## check that this is a git diff

    result = []
    file_text = []
    file_extension = ''

    # Some items are processed over several lines, such as struct = {
    # ... } name_of_struct; or int a, b, c = 3, d, e;
    current_topic = ''

    # general indent offset for text is 4 spaces
    offset = '    '

    end_marker = "+++ theEnd"
    inputfile.append(end_marker)

    # process every line in the file
    for i, line in enumerate(inputfile):

        ##### State variables for this loop ###############################

        words = line.split()

        action = None

        if not line:
            continue


        ###### Next file in patch
        ##
        ## Dealing with a new file in the patch: change of filename
        ## This makes the " * path/to/file: "   entry
        ##
        if line.find('---', 0,3) == 0 or line.find('+++', 0,3) == 0 :
            # check that the previous line wasn't a '---' or '+++'
            if file_extension != '':
                if file_extension in ['c','h']: #problem: cpp and c both share .h extensions
                    result.extend(c_section.generator(file_text, current_topic, summary))
                elif file_extension in ['txt']: #problem: cpp and c both share .h extensions
                    if "CMakeLists" in current_topic:
                        result.extend(cmake_section.generator(file_text, current_topic, summary))
                    else:
                        result.extend("(Undefined Changes):.")
                elif file_extension in ['py']: #
                    result.extend(python_section.generator(file_text, current_topic, summary))
                file_text = []
            if len(line) > 3 and current_topic != words[1][2:] and line != end_marker:
                current_topic = words[1][2:]
                file_extension = current_topic.rpartition('.')[2]
                result.append('\n\n  * '+ words[1][2:]+':\n')
            continue
        else:
            file_text.append(line)

    ## END of for i, line in enumerate(inputfile): main loop

    # Flag functions whose parameterlists were modified.
    curr = ""
    previ = ""
    for i, line in enumerate(result):
        curr = line[line.find("(")+1:line.find(")")]

        if 'Removed' in line or 'New' in line:
            if previ == "":
                previ = curr
            else:
                if previ == curr:
                    result[i-1] = ""
                    result[i] = ('\n' + offset +
                                 '(' + previ +
                                 '): Parameter list was modified.\n')
                    previ = ""
        else:
            previ = ""

        # remove duplicate entries
        if result[i-1] == result[i]:
            result[i-1] = ""

    return ''.join(result)
